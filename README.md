<h1 style=" width:100%;display:flex;align-items:center;justify-content:space-between;">Fragata: Ultimate Halo Experience <img src="repo_assets/logo.png" alt="drawing" width="100"/></h1>

### Concept Images

<p float="left">
 <img src="repo_assets/ServerDetailConcept.png" alt="Server detail concept" width="400"/>
<img src="repo_assets/ServerBrowserConcept.png" alt="Server browser concept" width="400"/>
<img src="repo_assets/MainMenuConcept.png" alt="Main menu concept" width="400"/>
</p>

## Development

First, clone the repo via git and install dependencies:

```bash
git clone https://git.haloss1.com/Fragata/fragata-client.git
cd fragata-client
yarn
```

## Starting Development Environment

Start the app in the `dev` environment:

```bash
yarn start
```

## Packaging for Production

To package apps for the local platform:

```bash
yarn package
```
