import React from 'react';
import { Link } from 'react-router-dom';
import { ReactComponent as Logo } from 'assets/fragata-logo.svg';

const Main = () => {
  return (
    <div style={{ height: '100vh' }}>
      <Link
        style={{
          position: 'fixed',
          bottom: '50vh',
          right: '2vw',
          fontSize: '2.5vw',
        }}
        to="/serverbrowser"
      >
        Server browser
      </Link>

      <Logo
        style={{
          fill: 'white',
          position: 'fixed',
          bottom: '8vh',
          left: '5vw',
          width: '40vw',
        }}
      />
      <div
        style={{
          position: 'fixed',
          bottom: '0',
          left: '0.5vw',
        }}
      >
        Version: 1.0.0-alpha.2
      </div>
    </div>
  );
};

export default Main;
