import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Gamedig from 'gamedig';
import Table from 'components/Table/Table';

const fs = require('fs');

const tempAddressArray = fs
  .readFileSync(`${__dirname}/assets/halom.gsl`)
  .toString()
  .split('\n');

const ServerBrowser = () => {
  const [serverInfo, setServerInfo] = useState(Array);
  const [isFetching, setIsFetching] = useState(true);

  const fetchServerInfo = async (addressArray: Array<string>) => {
    try {
      setIsFetching(true);
      const response = await Promise.all(
        addressArray
          .map((i: string) => i.split(':'))
          .map((entry: Array<string>) =>
            Gamedig.query({
              type: 'halo',
              host: entry[0],
              port: Number(entry[1]),
              maxAttempts: 0,
            })
              .then((state) => state)
              .catch(() => {
                // console.log('Server is offline', error);
              })
          )
      );
      setServerInfo(response.filter((e) => e !== undefined));
      setIsFetching(false);
    } catch (e) {
      // console.log(e);
    }
  };

  useEffect(() => {
    fetchServerInfo(tempAddressArray);

    return () => {
      setServerInfo([]);
    };
  }, []);

  return (
    <div>
      <div
        style={{
          position: 'sticky',
          top: 0,
          background:
            'linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(255,255,255,0) 100%)',
          padding: '1vh 1vw',
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <div style={{ fontSize: '3vw' }}>Server Browser</div>
          <button
            type="button"
            onClick={() => fetchServerInfo(tempAddressArray)}
            style={{ fontSize: '2vw' }}
          >
            Refresh
          </button>
        </div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <Link style={{ fontSize: '2vw' }} to="/">
            ❮ Back
          </Link>
          <p style={{ fontSize: '2vw' }}>Total Servers: {serverInfo.length}</p>
          <p style={{ fontSize: '2vw' }}>
            Total Players:{' '}
            {serverInfo.reduce(
              (accumulator, currentValue) =>
                accumulator + currentValue.players.length,
              0
            )}
          </p>
        </div>
      </div>
      {isFetching ? (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          Fetching Servers...
        </div>
      ) : (
        <Table serverInfo={serverInfo} />
      )}
    </div>
  );
};

export default ServerBrowser;
