import React from 'react';
import PropTypes from 'prop-types';

const Table = ({ serverInfo }: { serverInfo: [] }) => {
  return (
    <table style={{ width: '100%', padding: '0 1vw' }}>
      <tr style={{ textAlign: 'left' }}>
        <th>Server Name</th>
        <th>Map</th>
        <th>Variant</th>
        <th>Password</th>
        <th>Players</th>
        <th>Ping</th>
        <th>Address</th>
      </tr>
      {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        serverInfo.map((server: any) => (
          <tr key={server.connect}>
            <td>
              {server.name.replace(/[^a-zA-Z0-9/ ]/g, '').length === 0 ? (
                <span style={{ color: 'red' }}>FIX YOUR SERVER NAME!</span>
              ) : (
                server.name.replace(/[^a-zA-Z0-9/ ]/g, ' ').trim()
              )}
            </td>
            <td>{server.map}</td>
            <td>{server.raw.gametype}</td>
            <td>{server.password ? 'Yes' : 'No'}</td>
            <td>{`${server.players.length} / ${server.maxplayers}`}</td>
            <td>{`${server.ping} ms`}</td>
            <td>{server.connect}</td>
          </tr>
        ))
      }
    </table>
  );
};

Table.propTypes = {
  serverInfo: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default Table;
