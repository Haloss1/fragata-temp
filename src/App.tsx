/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Main from 'pages/Main/Main';
import ServerBrowser from 'pages/ServerBrowser/ServerBrowser';

import './App.global.css';

export default function App() {
  return (
    <>
      <video
        style={{
          objectFit: 'cover',
          width: '100vw',
          height: '100vh',
          position: 'fixed',
          top: 0,
          left: 0,
          zIndex: -1,
        }}
        autoPlay
        muted
        loop
        id="myVideo"
      >
        <source
          src="https://ipfs.io/ipfs/QmXEQBPoW7uEzbmbYepFjQ2hjL2SQPqVLkUysDyxVudPgc?filename=Halo%20CE.webm"
          type="video/webm"
        />
      </video>

      <audio
        id="music"
        autoPlay
        src="https://ipfs.io/ipfs/QmeSk2eh8xiFtth71rop3DH2F8h5NPn2FaNHHQYM2ruM2Z?filename=Halo%20Reach%20Beta.ogg"
        loop
      />
      <Router>
        <Switch>
          <Route path="/serverbrowser" component={ServerBrowser} />
          <Route path="/" component={Main} />
        </Switch>
      </Router>
    </>
  );
}
